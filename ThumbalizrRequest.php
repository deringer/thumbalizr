<?php
/**
 * thumbalizr.com API class
 *
 * forked from mptre/thumbalizr
 *
 * @author     Michael Dyrynda <deringer@iatstuti.net>
 * @package    thumbalizr
 */
// Prevent direct / incorrect access to class
if ( ! defined( '_THUMBALIZR' ) ) {
    die( 'No access' );
}

/**
 * thumbalizr.com API class
 *
 * @package    thumbalizr
 */
class ThumbalizrRequest {
    private $config;
    private $request_url;
    private $local_cache_subdir;
    private $local_cache_path;
    private $local_cache_file;
    private $headers;
    private $img;
    
    /**
     * Class constructor
     *
     * @access public
     * @param array $config Configuration array to be passed in
     * @return void
     */
    public function __construct( $config = array() ) {
        if ( ! is_array( $config ) ) {
            throw new ThumbalizrException( 'Configuration must be supplied as an array' );
        }

        if ( ! isset( $config['api_key'] ) || trim( $config['api_key'] ) == '' ) {
            throw new ThumbalizrException( 'api_key must be specified' );
        }
        
        // Build the configuration array
        $this->config = array(
            // Equivalent to $thumbalizr_config.
            'api_key'               => $this->set_config_value( 'api_key', $config, '' ),
            'service_url'           => $this->set_config_value( 'service_url', $config, 'http://api.thumbalizr.com/' ),
            'use_local_cache'       => $this->set_config_value( 'use_local_cache', $config, true ),
            'local_cache_dir'       => $this->set_config_value( 'local_cache_dir', $config, 'cache' ),
            'local_cache_expire'    => $this->set_config_value( 'local_cache_expire', $config, 300 ),
            // Equivalent to $thumbalizr_defaults.
            'width'                 => $this->set_config_value( 'width', $config, 250 ),
            'delay'                 => $this->set_config_value( 'delay', $config, 8 ),
            'encoding'              => $this->set_config_value( 'encoding', $config, 'png' ),
            'quality'               => $this->set_config_value( 'quality', $config, 90 ),
            'bwidth'                => $this->set_config_value( 'bwidth', $config, 1280 ),
            'bheight'               => $this->set_config_value( 'bheight', $config, 1024 ),
            'mode'                  => $this->set_config_value( 'mode', $config, 'screen' )
        );
    }


    /**
     * Check the configuration key exists in the user-provided config array, use the default otherwise
     *
     * @access private
     * @param string $key Config key to check for
     * @param array $config Configuration array to search through
     * @param mixed $default Default value to use if config key not present
     * @return mixed
     */
    private function set_config_value( $key, $config, $default ) {
        if ( isset( $config[$key] ) ) {
            return $config[$key];
        }

        return $default;
    }


    /**
     * Build the request URL
     *
     * @access private
     * @param string $url URL to obtain screenshot of
     * @return void
     */
    private function build_request( $url ) {
        $http_query_values = array(
            'api_key'   => $this->config['api_key'],
            'quality'   => $this->config['quality'],
            'width'     => $this->config['width'],
            'encoding'  => $this->config['encoding'],
            'delay'     => $this->config['delay'],
            'mode'      => $this->config['mode'],
            'bwidth'    => $this->config['bwidth'],
            'bheight'   => $this->config['bheight'],
            'url'       => $url,
        );

        $this->set_request_url( 
            sprintf( '%s?%s', $this->config['service_url'], http_build_query( $http_query_values ) )
        );
    }


    /**
     * Set local cache, if required
     *
     * @access private
     * @param string $url URL to set cache for
     * @return void
     */
    private function set_local_cache( $url ) {
        // Don't do any unnecessary processing
        if ( $this->config['use_local_cache'] ) {
            $this->local_cache_subdir = sprintf(
                '%s/%s',
                $this->config['local_cache_dir'],
                substr( md5( $url ), 0, 2 )
            );
            $this->local_cache_path = sprintf(
                '%s/%s_%d_%d_%d_%d_%d.%s',
                substr( md5( $url ), 0, 2 ),
                md5( $url ),
                $this->config['bwidth'],
                $this->config['bheight'],
                $this->config['delay'],
                $this->config['quality'],
                $this->config['width'],
                $this->config['encoding']
            );
            $this->local_cache_file = sprintf( '%s/%s', $this->config['local_cache_dir'], $this->local_cache_path );
        }
    }


    /**
     * Retrieve the local cache file
     *
     * @access public
     * @return string
     */
    public function get_local_cache_file() {
        return $this->local_cache_file;
    }


    /**
     * Retrieve the local cache path
     *
     * @access public
     * @return string
     */
    public function get_local_cache_path() {
        return $this->local_cache_path;
    }


    /**
     * Set the request URL
     *
     * @access private
     * @param string $url URL to set
     * @return void
     */
    private function set_request_url( $url ) {
        $this->request_url = $url;
    }


    /**
     * Perform the thumbalizr request on a given URL
     *
     * @access public
     * @param string $url URL to perform request on
     * @return void
     */
    public function request( $url ) {
        $this->build_request( $url );
        $this->set_local_cache( $url );
        
        if ( file_exists( $this->local_cache_file ) ) {
            $cachetime = time() - filemtime( $this->local_cache_file ) - $this->config['local_cache_expire'];
        } else {
            $cachetime = -1;
        }
        
        if ( ! file_exists( $this->local_cache_file ) || $cachetime >= 0 ) {
            // If the file doesn't exist, or the cache has expired, re-save the file
            $this->img = @file_get_contents( $this->request_url );

            $headers = array();

            foreach ( $http_response_header as $header ) {
                if ( strpos( $header, 'X-Thumbalizr-' ) !== false ) {
                    $tmp1 = explode( 'X-Thumbalizr-', $header );
                    $tmp2 = explode(': ',$tmp1[1]);
                    $headers[$tmp2[0]] = $tmp2[1];
                }
            }
            
            $this->headers = $headers;
            $this->save();
        } else {
            // Otherwise, just load the cached file
            $this->img = @file_get_contents($this->local_cache_file);
            $this->headers['URL'] = $url;
            $this->headers['Status'] = 'LOCAL';
        }
    }


    /**
     * Save the image stream to the local cache
     *
     * @access private
     * @return void
     */
    private function save() {
        if ( $this->img &&
             $this->config['use_local_cache'] === true &&
             isset($this->headers['Status']) &&
             $this->headers['Status'] == 'OK'
        ) {
            if ( ! file_exists( $this->local_cache_subdir ) ) {
                mkdir( $this->local_cache_subdir );
            }
            
            $fp = fopen( $this->local_cache_file, 'w' );
            fwrite( $fp, $this->img );
            fclose( $fp );
        }
    }


    /**
     * Dump the image stream to the browser
     *
     * @access public
     * @param bool $sendHeader If true, send the Content-type header
     * @return void
     */
    public function output( $sendHeader = true ) {
        if ( $this->img ) {
            if ( $sendHeader ) {
                if ( $this->config['encoding'] == 'jpg' ) {
                    header( 'Content-type: image/jpeg' );
                }
                else {
                    header( 'Content-type: image/png' );
                }
                
                foreach ( $this->headers as $key => $val ) {
                    header( 'X-Thumbalizr-' . $key . ': ' . $val );
                }
            }
            
            print $this->img;
        }
        else {
            return false;
        }
    }


    /**
     * Get the HTTP response headers
     *
     * @access public
     * @return array
     */
    public function getHeaders() {
        return $this->headers;
    }


    /**
     * Class destructor
     *
     * @access public
     * @return void
     */
    public function __destruct() {
        $this->config               = null;
        $this->request_url          = null;
        $this->local_cache_subdir   = null;
        $this->local_cache_path     = null;
        $this->local_cache_file     = null;
        $this->headers              = null;
        $this->img                  = null;
    }


}

/**
 * Empty exception class for use with ThumbalizrRequest
 *
 * @package    thumbalizr
 */
class ThumbalizrException extends Exception { }
